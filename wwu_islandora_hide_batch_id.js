// replace IBU batch IDs with nothing in search results page
// they look like this: 1570658038883 -  a 13 digit number (timestamp)

jQuery( document ).ready(function() {

    if (document.getElementsByClassName('islandora-solr-collection-display')) {

        var regex = /^\d{13}(\,*|$)/;                   // find 13 digit strings, or strings that begin with a 13 digit string followed by a comma

        var result_items = document.getElementsByClassName('islandora-object-description');
        for (var i = 0; i < result_items.length; i++) {
            var str = result_items[i].innerText;
            var text = str.replace(regex, "");
            result_items[i].innerText = text;
        }
    }

});