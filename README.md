# Overview
The Islandora Batch Uploader (IBU) adds a timestamp to the MODS of each record being uploaded, and that Batch ID appears in the search results because of the default MODS to Dublin Core translation.  This module finds that 13-digit Batch ID and hides it from the search results via JavaScript.

# Requirements
* Islandora 7.1.12+ 

# Installation
- navigate to your modules directory (usually /var/www/drupal/sites/all/modules/)
- clone this repository into that folder (e.g, 
git clone https://bitbucket.org/wwulibraries/wwu_islandora_hide_batch_id.git)
- drush en wwu_islandora_hide_batch_id

# Author: 
David Bass @ WWU

# License: 
MIT